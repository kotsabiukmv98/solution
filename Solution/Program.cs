﻿using System;

namespace Solution
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var solution = new Solution();

            var array = new int[] {3, 4, 3, 2, 3, -1, 3, 3};
            var result = solution.solution(array);
            PrintResult(array, result);

            array = null;
            result = solution.solution(array);
            PrintResult(array, result);

            array = new int[] { };
            result = solution.solution(array);
            PrintResult(array, result);

            array = new int[] { 0 };
            result = solution.solution(array);
            PrintResult(array, result);

            array = new int[] { 1, 2};
            result = solution.solution(array);
            PrintResult(array, result);

            array = new int[] { -4, -3, -2, -1, 0, 1, 2, 3, 4 };
            result = solution.solution(array);
            PrintResult(array, result);

            array = new int[] { 1, 2, 1 };
            result = solution.solution(array);
            PrintResult(array, result);
        }

        private static void PrintResult(int[] array, int result)
        {
            if (array is null)
            {
                Console.WriteLine("Array is NULL!!!");
                return;
            }
            Console.WriteLine("In array:");
            foreach (var item in array)
            {
                Console.Write($"{item}  ");
            }
            
            Console.WriteLine($"\nIndex of dominant element is: {result}\n\n");
        }
    }
}
