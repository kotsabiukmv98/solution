﻿using System.Collections.Generic;

namespace Solution
{
    public class Solution
    {
        public int solution(int[] A)
        {
            if (A is null)
                return -1;

            var N = A.Length;
            var candidateIndex = PotentialDominant(A, N);

            if (IsDominantElement(A, N, candidateIndex))
            {
                return candidateIndex;
            }
            else
            {
                return -1;
            }
        }

        private int PotentialDominant(IReadOnlyList<int> A, int N)
        {
            var dominantIndex = 0;
            var count = 1;

            for (var i = 1; i < N; i++)
            {
                if (A[dominantIndex] == A[i])
                {
                    count++;
                }
                else
                {
                    count--;
                }

                if (count == 0)
                {
                    dominantIndex = i;
                    count = 1;
                }
            }
            return dominantIndex;
        }

        private bool IsDominantElement(IReadOnlyList<int> A, int N, int candidateIndex)
        {
            var count = 0;
            foreach (var element in A)
            {
                if (element == A[candidateIndex])
                {
                    count++;
                }
            }

            return count > N / 2;
        }
    }
}